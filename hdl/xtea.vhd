
library IEEE;

use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

use std.env.stop;

entity xtea is
generic (
  NUM_CYCLES : integer := 64
);
port (
  nED : in std_logic;
  key : in std_logic_vector(127 downto 0);
  din : in std_logic_vector(63 downto 0);
  dout : out std_logic_vector(63 downto 0)
);
end xtea;

architecture architecture_xtea of xtea is
  component xtea_cell is
    Port (
      v   : in std_logic_vector(63 downto 0);
      key : in std_logic_vector(127 downto 0);
      sum : in std_logic_vector(31 downto 0);
      nED : in std_logic; -- not-encrypt/decrypt
      v_o : out std_logic_vector(63 downto 0);
      s_o : out std_logic_vector(31 downto 0)
    );
  end component;

  signal sum_out : std_logic_vector(31 downto 0);
  type value_t is array(0 to NUM_CYCLES-1) of std_logic_vector(63 downto 0);
  type sum_t is array(0 to NUM_CYCLES-1) of std_logic_vector(31 downto 0);
  signal tmp_v : value_t;
  signal tmp_s : sum_t;
  signal sum_val : std_logic_vector(31 downto 0);
  
  constant DELTA : std_logic_vector(31 downto 0) := x"9E3779B9";
begin
  
  -- set the starting sum-value
  process(nED)
    variable erg : std_logic_vector(63 downto 0);
  begin
    if nED = '1' then
      erg := std_logic_vector(unsigned(DELTA)*NUM_CYCLES);
      sum_val <= erg(31 downto 0);
    else
      sum_val <= (others => '0');
    end if;
  end process;
  
  -- generate the whole pipeline. First block gets the inputs.
  -- The outputs are fetched from the last array entry
  GEN_XTEA: for I in 0 to NUM_CYCLES-1 generate
    start_block: if I = 0 generate
      x0 : xtea_cell port map (
        v => din,
        key => key,
        sum => sum_val,
        nED => nED,
        v_o => tmp_v(0),
        s_o => tmp_s(0)
      );
    end generate;
    
    other_blocks: if I > 0 generate
      xN : xtea_cell port map (
        v => tmp_v(I-1),
        key => key,
        sum => tmp_s(I-1),
        nED => nED,
        v_o => tmp_v(I),
        s_o => tmp_s(I)
      );
    end generate;
  end generate GEN_XTEA;
  
  dout <= tmp_v(NUM_CYCLES-1);
  sum_out <= tmp_s(NUM_CYCLES-1);
  
end architecture_xtea;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity xtea_cell is
  Port (
    v   : in std_logic_vector(63 downto 0);
    key : in std_logic_vector(127 downto 0);
    sum : in std_logic_vector(31 downto 0);
    nED : in std_logic; -- not Encipher/Decipher
    v_o : out std_logic_vector(63 downto 0);
    s_o : out std_logic_vector(31 downto 0)
  );
end xtea_cell;

architecture behave of xtea_cell is
  signal v0, v1, v0_o, v1_o : std_logic_vector(31 downto 0);
  signal key_v0, key_v1 : std_logic_vector(31 downto 0);
  signal sum_delta, sum_delta_minus, shift_sum : std_logic_vector(31 downto 0);
  signal idx_v0, idx_v1 : integer;
  
  constant DELTA : std_logic_vector(31 downto 0) := x"9E3779B9";
begin
  v0 <= v(31 downto 0);
  v1 <= v(63 downto 32);
  
  -- select sum value for encryption/decryption
  sum_delta <= std_logic_vector(unsigned(sum) + unsigned(DELTA));
  sum_delta_minus <= std_logic_vector(unsigned(sum) - unsigned(DELTA));
  with nED select s_o <= sum_delta_minus when '1', sum_delta when others;
  
  idx_v0 <= to_integer(unsigned(sum_delta_minus and x"00000003")) when nED = '1' else to_integer(unsigned(sum and x"00000003"));
  shift_sum <= ("00000000000" & sum(31 downto 11)) when nED = '1' else ("00000000000" & sum_delta(31 downto 11));
  idx_v1 <= to_integer(unsigned(shift_sum and x"00000003"));
  key_v0 <= key(32*idx_v0 + 31 downto 32*idx_v0);
  key_v1 <= key(32*idx_v1 + 31 downto 32*idx_v1);
  
  
  process(v0, v1, key_v0, key_v1, sum_delta, sum)
    variable tmp_v01, tmp_v02, tmp_v0 : std_logic_vector(31 downto 0);
    variable tmp_v11, tmp_v12, tmp_v1 : std_logic_vector(31 downto 0);
  begin
    if nED = '1' then
      tmp_v11 := std_logic_vector(unsigned((v0(27 downto 0) & "0000") xor ("00000" & v0(31 downto 5))) + unsigned(v0));
      tmp_v12 := std_logic_vector(unsigned(sum) + unsigned(key_v1));
      tmp_v1 := std_logic_vector(unsigned(v1) - unsigned(tmp_v11 xor tmp_v12));
      v1_o <= tmp_v1;
      
      tmp_v01 := std_logic_vector(unsigned((tmp_v1(27 downto 0) & "0000") xor ("00000" & tmp_v1(31 downto 5))) + unsigned(tmp_v1));
      tmp_v02 := std_logic_vector(unsigned(sum_delta_minus) + unsigned(key_v0));
      tmp_v0 := std_logic_vector(unsigned(v0) - unsigned(tmp_v01 xor tmp_v02));
      v0_o <= tmp_v0;
    else
      tmp_v01 := std_logic_vector(unsigned((v1(27 downto 0) & "0000") xor ("00000" & v1(31 downto 5))) + unsigned(v1));
      tmp_v02 := std_logic_vector(unsigned(sum) + unsigned(key_v0));
      tmp_v0 := std_logic_vector(unsigned(v0) + unsigned(tmp_v01 xor tmp_v02));
      v0_o <= tmp_v0;
      
      tmp_v11 := std_logic_vector(unsigned((tmp_v0(27 downto 0) & "0000") xor ("00000" & tmp_v0(31 downto 5))) + unsigned(tmp_v0));
      tmp_v12 := std_logic_vector(unsigned(sum_delta) + unsigned(key_v1));
      tmp_v1 := std_logic_vector(unsigned(v1) + unsigned(tmp_v11 xor tmp_v12));
      v1_o <= tmp_v1;
    end if;
    
  end process;
  
  v_o <= v1_o & v0_o;
end behave;
