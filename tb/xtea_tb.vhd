
library IEEE;

use IEEE.std_logic_1164.all;

use std.env.stop;
use std.textio.all;

entity xtea_tb is
end xtea_tb;

architecture architecture_xtea_tb of xtea_tb is
  signal clk : std_logic := '0';
  signal key : std_logic_vector(127 downto 0);
  signal din, dout : std_logic_vector(63 downto 0) := (others => '0');
  signal nED : std_logic := '0';
  
  constant SOURCE_FILE : string := "H:\source.txt";
  constant ENCRYPTED_FILE : string := "H:\target_enc.txt";
  constant DECRYPTED_FILE : string := "H:\target_dec.txt";
begin
  
  clk <= not clk after 5 ns;
  
  -- key <= x"00000078_00000056_00000034_00000012";
  key <= x"22ce4f27_3776d748_ffc296ad_d7346871";
  
  -- this process encrypts a file and then decrypts it again
  process is
    variable line_v, line_o : line;
    file read_file : text;
    file write_file : text;
    variable slv_v : std_logic_vector(63 downto 0);
  begin
    nED <= '0';
    file_open(read_file, SOURCE_FILE, read_mode);
    file_open(write_file, ENCRYPTED_FILE, write_mode);
    while not endfile(read_file) loop
      readline(read_file, line_v);
      hread(line_v, slv_v);
      report "slv_v: " & to_hstring(slv_v);
      din <= slv_v;
      wait until rising_edge(clk);
      report "slv_v encrypted: " & to_hstring(dout);
      hwrite(line_o, dout);
      writeline(write_file, line_o);
    end loop;
    file_close(read_file);
    file_close(write_file);
    wait for 100 ns;
    report "Encryption finished";
    report "Starting decryption ... ";
    nED <= '1';
    
    file_open(read_file, ENCRYPTED_FILE, read_mode);
    file_open(write_file, DECRYPTED_FILE, write_mode);
    while not endfile(read_file) loop
      readline(read_file, line_v);
      hread(line_v, slv_v);
      report "slv_v: " & to_hstring(slv_v);
      din <= slv_v;
      wait until rising_edge(clk);
      report "slv_v encrypted: " & to_hstring(dout);
      hwrite(line_o, dout);
      writeline(write_file, line_o);
    end loop;
    file_close(read_file);
    file_close(write_file);
    report "Decryption finished";
    stop;
  end process;
  
  dut : entity work.xtea
  generic map (
    NUM_CYCLES => 64
  )
  port map (
    nED => nED,
    key => key,
    din => din,
    dout => dout
  );
end architecture_xtea_tb;
